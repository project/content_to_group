<?php

namespace Drupal\content_to_group\Form;

use Drupal\content_to_group\Util\ContentToGroupUtility;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class ContentToGroupSettingsForm extends ConfigFormBase {

  /**
   * The entityManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new object ContentToGroupSettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity Type Manager Service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'Content_to_group_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['content_to_group.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('content_to_group.settings');
    $contentToGroupUtility = new ContentToGroupUtility($this->entityTypeManager);
    $typeOptions = $contentToGroupUtility->getContentTypes();

    // Content types selection.
    $form['types'] = [
      '#type' => 'select',
      '#title' => $this->t('Select content types'),
      '#description' => $this->t('Select the content types to include.'),
      '#options' => !empty($typeOptions) ? $typeOptions : 'No content types',
      '#default_value' => $config->get('types'),
      '#multiple' => TRUE,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->configFactory->getEditable('content_to_group.settings');
    $config->set('types', $form_state->getValue('types'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
