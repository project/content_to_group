# Content to Group

The Content to Group module adds content to group using a referenced group
field, this is happen during the save or update of the content.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/content_to_group).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/content_to_group).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Group](https://www.drupal.org/project/group)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has a configuration form where we need to select the content types
we want to include.
`/admin/config/content-to-group-settings`


## Maintainers

- Daniel Cothran - [andileco](https://www.drupal.org/u/andileco)
- Mamadou Diao Diallo - [diaodiallo](https://www.drupal.org/u/diaodiallo)

**Supporting organization:**
- [John Snow, Inc. (JSI)](https://www.drupal.org/john-snow-inc-jsi)
